# [GNOME Search Provider][gnome-search-provider] for [gopass][] secrets

Because I manage my passwords in pass/gopass
and all the other extensions for that seemed broken!

## What's that logo?

It's the gopass logo.

## How do you install it?

### Arch Linux (btw)

Install [from the AUR][aur].

### Fedora

Install [from Copr][copr]:

	dnf copr enable mkhl/gnomish
	dnf install gnome-search-provider-gopass

### From Source

You'll need:

* GNOME
* Rust, Cargo etc.
* Make
* Sed

Build it with:

	make

Install it with:

	sudo make install

[Installing under `$HOME` **will not work**.][nohome]

[aur]: https://aur.archlinux.org/packages/gnome-search-provider-gopass
[copr]: https://copr.fedorainfracloud.org/coprs/mkhl/gnomish/
[gopass]: https://www.gopass.pw/
[gnome-search-provider]: https://developer.gnome.org/documentation/tutorials/search-provider.html
[nohome]: https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/3060
