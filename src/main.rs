use std::{
    error::Error,
    process::{Command, Stdio},
};

use futures_executor::block_on;
use search_provider::{ResultID, ResultMeta, SearchProvider, SearchProviderImpl};

const NAME: &str = "org.codeberg.mkhl.SearchProvider.Gopass";
const PATH: &str = "/org/codeberg/mkhl/SearchProvider/Gopass";

type Result<T> = core::result::Result<T, Box<dyn Error>>;

struct Application {}

fn gopass_list() -> Result<Vec<String>> {
    let result = Command::new("gopass")
        .arg("list")
        .arg("--flat")
        .stdin(Stdio::null())
        .output()?;
    let text = String::from_utf8(result.stdout)?;
    Ok(text.lines().map(Into::into).collect())
}

fn gopass_clip(secret: &ResultID) {
    Command::new("gopass")
        .arg("show")
        .arg("--clip")
        .arg(secret)
        .stdin(Stdio::null())
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .status()
        .expect("gopass failed");
}

impl SearchProviderImpl for Application {
    fn activate_result(&self, identifier: ResultID, _terms: &[String], _timestamp: u32) {
        gopass_clip(&identifier)
    }

    fn initial_result_set(&self, terms: &[String]) -> Vec<ResultID> {
        let previous_results = gopass_list().unwrap_or_default();
        self.subsearch_result_set(&previous_results, terms)
    }

    fn subsearch_result_set(
        &self,
        previous_results: &[ResultID],
        terms: &[String],
    ) -> Vec<ResultID> {
        previous_results
            .iter()
            .filter(|result| terms.iter().all(|term| result.contains(term)))
            .map(Into::into)
            .collect()
    }

    fn result_metas(&self, identifiers: &[ResultID]) -> Vec<ResultMeta> {
        identifiers
            .iter()
            .map(|identifier| ResultMeta::builder(identifier.clone(), identifier).build())
            .collect()
    }
}

async fn run() -> Result<()> {
    let app = Application {};
    SearchProvider::new(app, NAME, PATH).await?;
    Ok(())
}

fn main() -> Result<()> {
    block_on(run())
}
